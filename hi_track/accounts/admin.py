from django.contrib import admin
from django.contrib.auth import get_user_model
from .forms import BaseUserChangeForm, BaseUserCreationForm, CustomSetPasswordForm
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

USER_MODEL = get_user_model()


@admin.register(USER_MODEL)
class BaseUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', "phone_number")}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ("first_name", "last_name", 'email', 'phone_number', 'password1', 'password2'),
        }),
    )
    # The forms to add and change user instances
    form = BaseUserChangeForm
    add_form = BaseUserCreationForm
    # change_password_form = CustomSetPasswordForm
    list_display = ('pk', 'email', 'first_name', 'last_name',)
    list_filter = ('is_active', 'is_staff', 'groups')
    search_fields = ('first_name', 'last_name', 'email', 'phone_number')
    ordering = ('-date_joined',)
    filter_horizontal = ('groups', 'user_permissions',)
