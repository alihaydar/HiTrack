from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model
import logging

logger = logging.getLogger(__name__)

UserModel = get_user_model()


class CustomAuthBackend(ModelBackend):
    """
    This backend authenticates user by PIN or email
    """

    def authenticate(self, *args, username=None, password=None, **kwargs):
        logger.debug(username)
        logger.debug(password)
        if username.isdigit() and len(username) == 12:
            criterion = {'phone_number': username}
        else:
            criterion = {'email': username}

        try:
            user = UserModel.objects.get(**criterion)
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
