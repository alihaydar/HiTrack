from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import ReadOnlyPasswordHashField, PasswordChangeForm, AuthenticationForm, \
    PasswordResetForm, SetPasswordForm
from django.contrib.auth import (
    get_user_model, password_validation,
    authenticate)
from base_tools.custom_filter_methods import get_or_none
# from .tasks import ResetPasswordSendEmailTask
import logging

logr = logging.getLogger(__name__)

User = get_user_model()


class BaseUserCreationForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given phone number and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
                                widget=forms.PasswordInput(attrs={'placeholder': 'Password'}))
    password2 = forms.CharField(label=_("Password confirmation"),
                                widget=forms.PasswordInput(attrs={'placeholder': 'Re-enter Password'}),
                                help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = User
        fields = ("email", "first_name", "last_name", "phone_number")

    def __init__(self, *args, **kwargs):
        super(BaseUserCreationForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget = forms.TextInput(attrs={
            'placeholder': 'Email'})
        self.fields['first_name'].widget = forms.TextInput(attrs={
            'placeholder': 'First name'})
        self.fields['last_name'].widget = forms.TextInput(attrs={
            'placeholder': 'Last name'})
        self.fields['phone_number'].widget = forms.TextInput(attrs={
            'placeholder': 'Phone number *'})

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)
        return password2

    def save(self, commit=True):
        user = super(BaseUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class BaseUserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField(label=_("Password"),
                                         help_text=_("Raw passwords are not stored, so there is no way to see "
                                                     "this user's password, but you can change the password "
                                                     "using <a href=\"../password/\">this form</a>."))

    class Meta:
        model = User
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(BaseUserChangeForm, self).__init__(*args, **kwargs)
        f = self.fields.get('user_permissions', None)
        if f is not None:
            f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial["password"]


class BaseUserRegistrationForm(BaseUserCreationForm):
    accept = forms.BooleanField(initial=False,
                                label=_('I have read and agree to the Terms of Service and Privacy Policy.'),
                                error_messages={'required': _('You must accept the rules')})


class BaseAuthenticationForm(AuthenticationForm):
    pass


class BaseUserPasswordChangeForm(PasswordChangeForm):
    old_password = forms.CharField(label=_("Old password"),
                                   widget=forms.PasswordInput())

    new_password1 = forms.CharField(label=_("New password"),
                                    widget=forms.PasswordInput(),
                                    )
    new_password2 = forms.CharField(label=_("Confirm new password"),
                                    widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(BaseUserPasswordChangeForm, self).__init__(*args, **kwargs)
        self.fields['old_password'].widget = forms.PasswordInput(attrs={
            'placeholder': 'Old password:'})
        self.fields['new_password1'].widget = forms.PasswordInput(attrs={
            'placeholder': 'New password:'})
        self.fields['new_password2'].widget = forms.PasswordInput(attrs={
            'placeholder': 'Confirm new password:'})


class CustomSetPasswordForm(SetPasswordForm):
    pass


class ResetPasswordForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=254)
    phone_number = forms.EmailField(label=_("Email"), max_length=254)
    error_messages = {'unknown': _("This email address doesn't have an "
                                   "associated user account. Are you "
                                   "sure you've registered?")}

    def get_users(self, email):
        active_users = User.objects.filter(
            email__iexact=email, is_active=True)
        return (u for u in active_users if u.has_usable_password())

    def clean_email(self):
        email = self.cleaned_data['email']
        results = list(self.get_users(email))
        logr.debug(email)

        if not results:
            raise forms.ValidationError(self.error_messages['unknown'])
        return email

    # def save(self):
    #     email = self.cleaned_data["email"]
    #     task = ResetPasswordSendEmailTask()
    #     user = get_or_none(User, email=email)
    #     result = task.delay(user.pk).get()
    #     logr.debug(result)
