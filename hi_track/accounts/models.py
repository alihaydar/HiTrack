from django.core import validators
from django.db import models
from django.utils import timezone
from django.conf import settings
from django.core.validators import RegexValidator
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from .managers import CustomUserManager

USER_MODEL = settings.AUTH_USER_MODEL


class BaseUser(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField(_('first name'), max_length=255, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=255, blank=True, null=True)
    email = models.EmailField(_('email address'), max_length=255, unique=True, null=True, blank=True)
    phone_number = models.CharField(_('phone number'), max_length=25, unique=True)
    address = models.CharField(max_length=125, verbose_name=_("Address"), blank=True, null=True)

    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone_number'

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = "{} {}".format(self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Returns the short name for the user."""
        return self.first_name

    @property
    def is_repairer(self):
        groups = [group.name for group in self.groups.all()]
        return 'repairer' in groups

    @property
    def is_service_center_manager(self):
        groups = [group.name for group in self.groups.all()]
        return 'service_center_manager' in groups

    @property
    def is_azercell_manager(self):
        groups = [group.name for group in self.groups.all()]
        return 'azercell_manager' in groups

    @property
    def is_customer(self):
        groups = [group.name for group in self.groups.all()]
        return 'customer' in groups

    def __str__(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        elif self.email:
            return self.email
        else:
            return str(self.phone_number)
