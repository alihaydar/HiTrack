from celery import Task
import hashlib
import random
import redis
from datetime import timedelta
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.conf import settings
from django.urls import reverse
from base_tools.custom_filter_methods import get_or_none
from notifications.tasks import SendSMS, SendMail
import logging

logr = logging.getLogger(__name__)

UserModel = get_user_model()


class SendActivationCodeToUserPhoneNumberTask(Task):
    """
        This task create activation key and set it into redis. After task send this key to User phone
    """
    r = redis.Redis(connection_pool=settings.REDIS_POOL)

    def run(self, user_id):
        user = get_or_none(UserModel, id=user_id)
        if user:
            print(user)
            logr.debug(user)
            salt = hashlib.sha1(str(random.random()).encode("utf-8")).hexdigest()[:6]
            key = hashlib.sha256(str(user.phone_number + salt).encode("utf-8")).hexdigest()[:6]
            sent_sms = SendSMS()
            sent_sms(user.phone_number, key)
            if sent_sms:
                self.r.setex(key, user.phone_number, int(timedelta(seconds=100).total_seconds()))
                user.set_password(key)
                user.save()
        else:
            print("User does'not exist")


class SendActivationToUserEmailTask(Task):
    """
        This task create activation key and set it into redis. After task send this key to User mail
    """
    r = redis.Redis(connection_pool=settings.REDIS_POOL)
    email_template_name = "email-activation.html"

    def run(self, user_id):
        user = get_or_none(UserModel, id=user_id)
        if user and user.email:
            print(user)
            salt = hashlib.sha1(str(random.random()).encode("utf-8")).hexdigest()[:10]
            key = hashlib.sha256(str(user.email + salt).encode("utf-8")).hexdigest()
            self.r.setex(key, user.email, int(timedelta(days=3).total_seconds()))

            subject = "Confirmation of registration"
            txt = """Thank you for registration,
                        please click the following link in order to confirm your registration:
                        {link}?activation={key}
                        """.format(link="".join([settings.HOST, reverse('accounts:email_confirm')]), key=key)
            data = {
                "subject": subject,
                "message": txt,
                "to": user.email,
            }
            sent_mail = SendMail()
            sent_mail(data)
        else:
            print("User does'not exist")


# class ResetPasswordSendEmailTask(Task):
#     """
#         This task send password reset mail.
#     """
#     r = redis.Redis(connection_pool=settings.REDIS_POOL)
#     email_template_name = "email/password-reset-confirm.html"
#
#     def run(self, user_id):
#         return self.send_link_to_email(user_id)
#
#     def send_link_to_email(self, user_pk):
#         logr.debug("reset task")
#         user = get_or_none(UserModel, pk=user_pk)
#         if user:
#             salt = hashlib.sha1(str(random.random()).encode("utf-8")).hexdigest()[:6]
#             key = hashlib.sha256(str(user.phone_number + salt).encode("utf-8")).hexdigest()[:6]
#             token = salt + key + default_token_generator.make_token(user)
#             self.r.setex(token, user.email, int(timedelta(days=2).total_seconds()))
#             url = reverse("account:reset_password_confirm_api", kwargs={"token": token})
#             ctx = {
#                 "subject": "Password reset",
#                 "message": "{}{}".format(settings.HOST, url)
#             }
#             send_mail_task([user.email], with_mailgun=True, **ctx)
