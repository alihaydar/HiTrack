from django.conf.urls import url, include
from .views import LoginView, LogOutView, ProfileView, PasswordChangeView, EmailConfirm, RegistrationView, PhoneConfirm

urlpatterns = [
    url(r'^confirm/$', EmailConfirm.as_view(), name="email_confirm"),
    url(r'^phone-confirm/$', PhoneConfirm.as_view(), name="phone_confirm"),
    url(r'^login/$', LoginView.as_view(), name="login"),
    url(r'^profile/$', ProfileView.as_view(), name="profile"),
    url(r'^registration/$', RegistrationView.as_view(), name="registration"),
    url(r'^change-password/$', PasswordChangeView.as_view(), name="change_password"),
    # url(r'^reset-password/$', PasswordResetView.as_view(), name="reset_password"),
    # url(r'^reset-password-done/$', PasswordResetDoneView.as_view(), name="reset_password_done"),
    # url(r'^reset-password-confirm/(?P<token>[0-9A-Za-z_\-]+)/$', PasswordResetConfirmView.as_view(),
    #     name="reset_password_confirm"),
    url(r'^logout/$', LogOutView.as_view(), name="logout"),
]
