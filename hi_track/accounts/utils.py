from django.contrib.auth import logout
from django.contrib.sessions.models import Session


def logout_and_delete_all_session(request):
    all_sessions = Session.objects.all()
    request_user_pk = request.user.pk
    for session in all_sessions:
        user_id = session.get_decoded().get('_auth_user_id')
        if request_user_pk == user_id:
            session.delete()
    logout(request)


def logout_and_delete_other_sessions(request):
    current_session_key = request.session.session_key
    request_user_pk = request.user.pk
    other_sessions = Session.objects.exclude(session_key=current_session_key)
    for session in other_sessions:
        user_id = session.get_decoded().get('_auth_user_id')
        if request_user_pk == user_id:
            session.delete()
    logout(request)
