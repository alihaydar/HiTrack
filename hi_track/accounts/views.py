from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import RedirectView, FormView, TemplateView, CreateView, View
from django.http import HttpResponseBadRequest, HttpResponseForbidden, HttpResponse
from django.urls import reverse_lazy
from django.conf import settings
from django import forms as django_forms
from django.contrib.auth.models import Group
from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib.auth import get_user_model, authenticate, logout, login
from .forms import BaseAuthenticationForm, BaseUserPasswordChangeForm, BaseUserCreationForm
from .utils import logout_and_delete_other_sessions
from .tasks import SendActivationToUserEmailTask, SendActivationCodeToUserPhoneNumberTask
import redis

USER_MODEL = get_user_model()


class LoginView(FormView):
    form_class = BaseAuthenticationForm
    template_name = 'accounts/login.html'
    success_url = reverse_lazy("customer_side:repair_jobs_list")
    logout_url = reverse_lazy("accounts:logout")

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginView, self).form_valid(form)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(self.success_url)
        return super(LoginView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        next_url = self.request.GET.get('next', None)
        if next_url:
            if next_url == self.logout_url:
                return self.success_url
            return next_url
        else:
            return self.success_url


class LogOutView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy("accounts:login")
    login_url = reverse_lazy("accounts:login")

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogOutView, self).get(request, *args, **kwargs)


class PasswordChangeView(LoginRequiredMixin, FormView):
    form_class = BaseUserPasswordChangeForm
    template_name = 'accounts/profile.html'
    success_url = reverse_lazy("accounts:login")
    login_url = reverse_lazy("accounts:login")

    def get_form_kwargs(self):
        kwargs = super(PasswordChangeView, self).get_form_kwargs()
        kwargs['user'] = self.get_user()
        return kwargs

    def get_user(self):
        return self.request.user

    def form_valid(self, form):
        form.save()
        logout_and_delete_other_sessions(self.request)
        return super(PasswordChangeView, self).form_valid(form)


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "accounts/profile.html"
    form_class = BaseUserPasswordChangeForm
    login_url = reverse_lazy("accounts:login")

    def get_context_data(self, **kwargs):
        context_data = super(ProfileView, self).get_context_data(**kwargs)
        context_data["form"] = self.form_class(self.request.GET)
        return context_data


class RegistrationView(CreateView):
    model = USER_MODEL
    form_class = BaseUserCreationForm
    template_name = "accounts/registration.html"
    success_url = reverse_lazy("accounts:phone_confirm")
    home_page = reverse_lazy("customer_side:repair_jobs_list")

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(self.home_page)
        return super(RegistrationView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        self.object = form.save()
        self.object.is_active = False
        self.object.save()
        group, created = Group.objects.get_or_create(name="customer")
        self.object.groups.add(group)
        if self.object.phone_number:
            send_activation = SendActivationCodeToUserPhoneNumberTask()
            send_activation.delay(self.object.pk)
        elif self.object.email:
            send_activation = SendActivationToUserEmailTask()
            send_activation.delay(self.object.pk)
        return super(RegistrationView, self).form_valid(form)


class EmailConfirm(View):
    success_url = reverse_lazy("accounts:login")

    # resend_url = reverse_lazy("account:resend_activation_key")

    def get(self, request):
        r = redis.Redis(connection_pool=settings.REDIS_POOL)
        try:
            activation_key = django_forms.CharField().clean(request.GET.get("activation"))
            email = r.get(activation_key)
            if email:
                confirm_user = USER_MODEL.objects.get(email=email.decode("utf-8"))
                if confirm_user.is_active:
                    return HttpResponseForbidden()
                else:
                    confirm_user.is_active = True
                    confirm_user.save()
                    r.delete(activation_key)
                    return redirect(self.success_url)
            else:
                pass
                # return redirect(self.resend_url)
        except (USER_MODEL.DoesNotExist, ValueError, django_forms.ValidationError):
            return HttpResponseBadRequest()


class PhoneConfirm(View):
    success_url = reverse_lazy("accounts:login")
    template_name = "accounts/confirm.html"

    # resend_url = reverse_lazy("account:resend_activation_key")

    def get(self, request):
        return render(request, self.template_name, {})

    def post(self, request):
        r = redis.Redis(connection_pool=settings.REDIS_POOL)
        try:
            activation_key = request.POST.get("activation")
            phone_number = r.get(activation_key)
            if phone_number:
                confirm_user = USER_MODEL.objects.get(phone_number=phone_number.decode("utf-8"))
                if confirm_user.is_active:
                    return redirect(self.success_url)
                else:
                    confirm_user.is_active = True
                    confirm_user.save()
                    r.delete(activation_key)
                    return redirect(self.success_url)
            else:
                pass
                # return redirect(self.resend_url)
        except (USER_MODEL.DoesNotExist, ValueError, django_forms.ValidationError):
            return HttpResponseBadRequest()
