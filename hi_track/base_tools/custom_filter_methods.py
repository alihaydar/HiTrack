def get_or_none(m, *args, **kwargs):
    try:
        return m.objects.get(*args, **kwargs)
    except m.DoesNotExist:
        return None