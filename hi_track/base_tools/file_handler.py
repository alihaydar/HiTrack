from PIL import *
from django.core.files.utils import FileProxyMixin


class FileFilter:
    img_extensions = ['.jpg', '.png', '.jpeg']
    other_extensions = ['.pdf', '.doc', '.docx', '.txt']
    size_limit = 10000000000000

    def __init__(self, file):
        if isinstance(file, FileProxyMixin):
            self.file = file
        else:
            raise ValueError

    def is_valid_size(self):
        if self.file.size < self.size_limit:
            return True
        else:
            return False

    def is_valid(self):
        return self.is_valid_size() and (self.is_image() or self.is_other())

    def is_image(self):
        return any(self.file.name.endswith(ex) for ex in self.img_extensions)

    def is_other(self):
        return any(self.file.name.endswith(ex) for ex in self.other_extensions)
