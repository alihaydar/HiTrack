from time import time


class FileName:
    attachments_path = "hi-track/attachments/"

    @staticmethod
    def get_attachment_name(instance, filename):
        return "{}/{}_{}".format(FileName.attachments_path, str(time()).replace('.', '_'), filename)
