from django.utils.crypto import get_random_string


def generate_repair_job_number():
    return int(get_random_string(length=7, allowed_chars='1234567890'))
