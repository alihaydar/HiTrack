from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from .models import AzercellCenter, ServiceCenter, Repairer, RepairStatus, Device, RepairJob, RepairJobAttachments, \
    TimeLine, ProblemType
from base_tools.custom_filter_methods import get_or_none


class TimeLineInline(admin.TabularInline):
    model = TimeLine
    extra = 1


class RepairJobAttachmentsInline(admin.TabularInline):
    model = RepairJobAttachments
    extra = 1


@admin.register(ProblemType)
class ProblemTypeAdmin(admin.ModelAdmin):
    list_display = ("name", )
    search_fields = ("name", )


@admin.register(AzercellCenter)
class AzercellCenterAdmin(admin.ModelAdmin):
    list_display = ("name", "phone_number", "email", "center_type")
    search_fields = ("name", "phone_number", "email", "center_type")
    list_filter = ("center_type",)
    filter_horizontal = ("managers", "couriers")
    readonly_fields = ("update_at", "created_at")

    def get_queryset(self, request):
        user = request.user
        if user.is_superuser:
            return self.model.objects.all()
        else:
            return self.model.objects.none()


@admin.register(ServiceCenter)
class ServiceCenterAdmin(admin.ModelAdmin):
    list_display = ("name", "phone_number", "email",)
    search_fields = ("name", "phone_number", "email",)
    readonly_fields = ("update_at", "created_at")

    def get_queryset(self, request):
        user = request.user
        if user.is_superuser or user.is_azercell_manager:
            return self.model.objects.all()
        elif user.is_service_center_manager:
            return self.model.objects.filter(manager=request.user)
        else:
            return self.model.objects.none()

    #
    # def has_delete_permission(self, request, obj=None):
    #     user = request.user
    #     if user.is_azercell_manager:
    #         return True
    #     else:
    #         return False
    #
    def has_add_permission(self, request):
        user = request.user
        if user.is_azercell_manager or user.is_superuser:
            return True
        return False

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super(ServiceCenterAdmin, self).save_model(request, obj, form, change)


@admin.register(Repairer)
class RepairerAdmin(admin.ModelAdmin):
    readonly_fields = ("update_at", "created_at")

    def get_queryset(self, request):
        user = request.user
        if user.is_azercell_manager:
            return self.model.objects.all()
        else:
            return self.model.objects.none()

    # def has_delete_permission(self, request, obj=None):
    #     user = request.user
    #     if user.is_azercell_manager:
    #         return True
    #     else:
    #         return False
    #
    # def has_add_permission(self, request):
    #     user = request.user
    #     if user.is_azercell_manager:
    #         return True
    #     return False


@admin.register(RepairStatus)
class RepairStatusAdmin(admin.ModelAdmin):
    search_fields = ("name", "description")
    list_display = ("name",)
    readonly_fields = ("update_at", "created_at")

    def get_queryset(self, request):
        user = request.user
        if user.is_azercell_manager:
            return self.model.objects.all()
        else:
            return self.model.objects.none()

    # def has_delete_permission(self, request, obj=None):
    #     user = request.user
    #     if user.is_azercell_manager:
    #         return True
    #     else:
    #         return False
    #
    # def has_add_permission(self, request):
    #     user = request.user
    #     if user.is_azercell_manager:
    #         return True
    #     return False


@admin.register(Device)
class DeviceStatusAdmin(admin.ModelAdmin):
    search_fields = ("name", "os", "device_type")
    list_display = ("name", "os", "device_type")
    list_filter = ("os", "device_type")
    readonly_fields = ("update_at", "created_at")


@admin.register(RepairJob)
class RepairJobAdmin(admin.ModelAdmin):
    list_display = ("repairer_job_id", "problem_title", "device", "cost", "finish_date")
    search_fields = ("repairer_job_id", "problem_title")
    list_filter = ("confirmed_by_owner",)
    readonly_fields = ("update_at", "created_at")
    inlines = [
        TimeLineInline, RepairJobAttachmentsInline
    ]

    def get_queryset(self, request):
        user = request.user
        if user.is_azercell_manager:
            return self.model.objects.filter(azercell_center_worker=user)
        elif user.is_repairer:
            return self.model.objects.filter(repairer=user)
        else:
            return self.model.objects.all()

    # def has_delete_permission(self, request, obj=None):
    #     user = request.user
    #     return not (user.is_repairer and user.is_courier)

    # def has_add_permission(self, request):
    #     user = request.user
    #     return not(user.is_courier or user.is_repairer)

    def delete_model(self, request, obj):
        user = request.user
        if user.is_azercell_manager:
            return obj.delete()


admin.site.site_header = _('HiTrack administration')
admin.site.index_title = _('HiTrack administration')
admin.site.site_title = _('HiTrack')
