from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.forms.models import model_to_dict
from .options.choices import CENTER_TYPE_CHOICES, DEVICE_TYPE_CHOICES, OS_TYPE_CHOICES, ATTACHMENTS_TYPE_CHOICES
from base_tools.file_names import FileName
from base_tools.generate_random import generate_repair_job_number

USER_MODEL = settings.AUTH_USER_MODEL


class AzercellCenter(models.Model):
    name = models.CharField(max_length=55)
    address = models.CharField(max_length=125)
    phone_number = models.CharField(max_length=25)
    email = models.EmailField(blank=True, null=True)
    center_type = models.CharField(choices=CENTER_TYPE_CHOICES, max_length=25)
    managers = models.ManyToManyField(USER_MODEL, blank=True, related_name="managers")
    couriers = models.ManyToManyField(USER_MODEL, blank=True, related_name="couriers")
    rate = models.FloatField(default=0, verbose_name=_("Rate"))

    update_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class ServiceCenter(models.Model):
    name = models.CharField(max_length=55)
    address = models.CharField(max_length=125)
    phone_number = models.CharField(max_length=25)
    email = models.EmailField(blank=True, null=True)
    rate = models.FloatField(default=0, verbose_name=_("Rate"))
    manager = models.ForeignKey(USER_MODEL, verbose_name=_("Manager"), blank=True, null=True)
    created_by = models.ForeignKey(USER_MODEL, verbose_name=_("Created by"), related_name='created_service_centers', null=True)

    update_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Repairer(models.Model):
    user = models.OneToOneField(USER_MODEL)
    service_center = models.ForeignKey(ServiceCenter)
    rate = models.FloatField(default=0, verbose_name=_("Rate"))

    update_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} from {}".format(self.user, self.service_center)


class RepairStatus(models.Model):
    name = models.CharField(max_length=55)
    description = models.TextField(blank=True, null=True)

    update_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Device(models.Model):
    device_type = models.CharField(choices=DEVICE_TYPE_CHOICES, max_length=25)
    name = models.CharField(max_length=50, verbose_name=_("Name"))
    os = models.CharField(choices=OS_TYPE_CHOICES, max_length=55)

    update_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} {} {}".format(self.device_type, self.name, self.os)


class ProblemType(models.Model):
    name = models.CharField(max_length=125, verbose_name=_("Name"))
    description = models.TextField(blank=True, null=True, verbose_name=_("Description"))

    def __str__(self):
        return self.name


class RepairJob(models.Model):
    repairer_job_id = models.PositiveIntegerField(unique=True, default=generate_repair_job_number,
                                                  verbose_name=_("ID"))
    owner = models.ForeignKey(USER_MODEL, related_name="owner")
    azercell_center_worker = models.ForeignKey(USER_MODEL, related_name="creator", blank=True, null=True)
    azercell_center = models.ForeignKey(AzercellCenter, blank=True, null=True)
    repair_service = models.ForeignKey(ServiceCenter, verbose_name=_("Service Center"), blank=True, null=True)
    problem_title = models.CharField(max_length=125, verbose_name=_("Problem title"))
    problem_type = models.ForeignKey(ProblemType, verbose_name=_("Problem type"), blank=True, null=True)
    description = models.TextField(verbose_name=_("Description"))
    device = models.ForeignKey(Device)
    rate = models.PositiveIntegerField(verbose_name=_("Rate"), default=0)
    comment = models.TextField(verbose_name=_("Comment"), blank=True, null=True)
    cost = models.FloatField(verbose_name=_("Cost"), blank=True, null=True)
    confirmed_by_owner = models.BooleanField(default=False, verbose_name=_("Confirmed by owner"))
    finish_date = models.DateField(blank=True, null=True, verbose_name=_("Finish date"))
    is_active = models.BooleanField(default=True, verbose_name=_("Is job active?"))

    update_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(RepairJob, self).__init__(*args, **kwargs)
        self.old_object_state = model_to_dict(self)

    def __str__(self):
        return str(self.repairer_job_id)

    @property
    def current_status(self):
        if self.timeline_set.all():
            status = self.timeline_set.last().repair_status.name
            return status
        return "waiting"

    class Meta:
        permissions = (
            ("repair_job_view", "Can view Repair Job"),
            ("repair_job_all_view", "Can view ALL Repair Jobs"),
        )


class TimeLine(models.Model):
    repair_job = models.ForeignKey(RepairJob, verbose_name=_("Repair Job"))
    repair_status = models.ForeignKey(RepairStatus, verbose_name=_("Repair status"))
    created_at = models.DateTimeField(auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(TimeLine, self).__init__(*args, **kwargs)
        self.old_object_state = model_to_dict(self)


class RepairJobAttachments(models.Model):
    repair_job = models.ForeignKey(RepairJob, verbose_name=_("Repair job"))
    attachment_type = models.CharField(choices=ATTACHMENTS_TYPE_CHOICES, verbose_name=_("Attachment type"),
                                       max_length=25)
    file = models.FileField(upload_to=FileName.get_attachment_name, verbose_name=_("File"))
    description = models.TextField(verbose_name=_("Description"), blank=True, null=True)
