CENTER_TYPE_CHOICES = (
    ("dealer", "Dealer"),
    ("express", "Express"),
)

DEVICE_TYPE_CHOICES = (
    ("phone", "Phone"),
    ("tablet", "Tablet"),
    ("pc", "PC"),
    ("laptop", "Laptop"),
)

OS_TYPE_CHOICES = (
    ("android", "Android"),
    ("ios", "IOS"),
    ("windows", "Windows"),
)

ATTACHMENTS_TYPE_CHOICES = (
    ("warranty_card", "Warranty card"),
    ("id_card", "ID card"),
)
