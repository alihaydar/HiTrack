from django.db.models.signals import post_save
from django.db.models import Avg
from django.dispatch import receiver
from .models import RepairJob, Repairer


@receiver(post_save, sender=RepairJob)
def repair_job_post_save(created, sender, update_fields, instance=None, **kwargs):
    def set_rate(instance):
        if instance.repair_service:
            repair_service = instance.repair_service
            repair_service.rate = RepairJob.objects.filter(repair_service=repair_service).\
                aggregate(Avg("rate")).get("rate__avg")
            repair_service.save()
        if instance.azercell_center:
            instance.azercell_center.rate = RepairJob.objects.filter(azercell_center=instance.azercell_center).\
                aggregate(Avg("rate")).get("rate__avg")
            instance.azercell_center.save()

    old_object_state = instance.old_object_state

    if old_object_state.get('rate') != instance.rate:
        set_rate(instance)
