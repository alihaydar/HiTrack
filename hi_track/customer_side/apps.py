from django.apps import AppConfig


class CustomerSideConfig(AppConfig):
    name = 'customer_side'
