from django import forms
from core.models import RepairJob


class RepairJobForm(forms.ModelForm):
    class Meta:
        model = RepairJob
        fields = ("confirmed_by_owner", "rate", "comment")
