from django.conf.urls import url, include
from .views import RepairJobUpdateView, RepairJobListView, RepairAjaxView

urlpatterns = [
    url(r'^repair-jobs/$', RepairJobListView.as_view(), name="repair_jobs_list"),
    url(r'^repair-jobs/(?P<pk>[0-9]+)/$', RepairJobUpdateView.as_view(), name="repair_jobs_update"),
    url(r'^repair-jobs-ajax/(?P<pk>[0-9]+)/$', RepairAjaxView.as_view(), name="repair_jobs_ajax"),
]
