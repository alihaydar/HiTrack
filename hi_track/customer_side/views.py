from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from django.http.response import HttpResponse, JsonResponse

import json
from rest_framework.response import Response
from rest_framework import status
from django.views.generic import View, UpdateView, ListView
from django.core.urlresolvers import reverse, reverse_lazy
from core.models import RepairJob
from customer_side.forms import RepairJobForm
from base_tools.custom_filter_methods import get_or_none
import logging

logger = logging.getLogger(__name__)


class RepairJobListView(LoginRequiredMixin, ListView):
    model = RepairJob
    paginate_by = 10
    template_name = "customer_side/repair-info-list.html"
    login_url = reverse_lazy("accounts:login")
    context_object_name = "repair_jobs"

    def get_queryset(self):
        repair_jobs = self.model.objects.filter(owner=self.request.user)
        return repair_jobs


class RepairJobUpdateView(LoginRequiredMixin, UpdateView):
    model = RepairJob
    form_class = RepairJobForm
    template_name = "customer_side/repair-detail.html"
    login_url = reverse_lazy("accounts:login")
    success_url = reverse_lazy("customer_side:repair_jobs_list")
    context_object_name = "repair"
    renderer_classes = (JSONRenderer, TemplateHTMLRenderer)

    def get_queryset(self):
        repair_jobs = self.model.objects.filter(owner=self.request.user)
        return repair_jobs

    def put(self, request, pk):
        json_str = request.body.decode(encoding='UTF-8')
        data = json.loads(json_str)
        obj = get_object_or_404(RepairJob, pk=pk)
        logger.info('Repair Job Update {} {}'.format(data, obj))
        if data.get("rate"):
            obj.rate = data.get("rate")
        if data.get("comment"):
            obj.comment = data.get("comment")
        if data.get("confirmed_by_owner"):
            obj.confirmed_by_owner = data.get("confirmed_by_owner")
        obj.save()
        response = JsonResponse(status=200, data={'message': 'success'})
        # response.accepted_renderer = JSONRenderer
        # response.accepted_media_type =
        return response
        # try:
        #
        # except:
        #     return HttpResponse(status=400, content_type="application/json")


class RepairAjaxView(LoginRequiredMixin, View):

    def put(self, request, pk):
        try:
            json_str = request.body.decode(encoding='UTF-8')
            data = json.loads(json_str)
            obj = get_object_or_404(RepairJob, pk=pk)
            if data.get("rate"):
                obj.rate = data.get("rate")
            if data.get("comment"):
                obj.comment = data.get("comment")
            if data.get("confirmed_by_owner"):
                obj.confirmed_by_owner = data.get("confirmed_by_owner")
            return Response(status=status.HTTP_200_OK)
        except:
            return HttpResponse(status=400, content_type="application/json")
