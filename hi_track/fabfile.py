from fabric.api import run
from fabric.operations import local as lrun, run
from fabric.api import task
from fabric.state import env


def restart(pip=False, migrate=False):
    if pip:
        lrun('pip install -r requirements.txt')
    if migrate:
        lrun('./manage.py makemigrations')
        lrun('./manage.py migrate')
    lrun('sudo systemctl restart hi_track-gunicorn.service')
    lrun('sudo systemctl restart nginx')
    lrun('sudo systemctl restart redis')
    # lrun('sudo systemctl restart hi_track-gunicorn.service')
