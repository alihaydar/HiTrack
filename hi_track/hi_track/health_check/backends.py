import logging
from timeit import default_timer as timer
from django.utils.translation import ugettext_lazy as _
from django.db import DatabaseError, IntegrityError
from .exceptions import HealthCheckException, ServiceReturnedUnexpectedResult, ServiceUnavailable
from core.models import RepairStatus

logger = logging.getLogger('health-check')


class BaseHealthCheckBackend:
    def __init__(self):
        self.errors = []

    def check_status(self):
        raise NotImplementedError

    def run_check(self):
        start = timer()
        self.errors = []
        try:
            self.check_status()
        except HealthCheckException as e:
            self.add_error(e, e)
        except BaseException:
            logger.exception("Unexpected Error!")
            raise
        finally:
            self.time_taken = timer() - start

    def add_error(self, error, cause=None):
        if isinstance(error, HealthCheckException):
            pass
        elif isinstance(error, str):
            msg = error
            error = HealthCheckException(msg)
        else:
            msg = _("unknown error")
            error = HealthCheckException(msg)
        if isinstance(cause, BaseException):
            logger.exception(str(error))
        else:
            logger.error(str(error))
        self.errors.append(error)

    def pretty_status(self):
        if self.errors:
            return "\n".join(str(e) for e in self.errors)
        return _('working')

    @property
    def status(self):
        return int(not self.errors)

    def identifier(self):
        return self.__class__.__name__


class DatabaseCheckBackend(BaseHealthCheckBackend):
    def check_status(self):
        try:
            obj = RepairStatus.objects.create(name="test")
            obj.title = "newtest"
            obj.save()
            obj.hard_delete()
            return True
        except IntegrityError:
            raise ServiceReturnedUnexpectedResult("Integrity Error")
        except DatabaseError:
            raise ServiceUnavailable("Database error")


class CheckAllBackends:
    backends = [DatabaseCheckBackend, ]

    def check(self):
        try:
            return all([backend().check_status() for backend in self.backends])
        except Exception as ex:
            return False

