from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .backends import CheckAllBackends


class HealthCheckAPIView(APIView):
    def get(self, request):
        checker = CheckAllBackends()
        if checker.check():
            return Response(data={"message": "Everything is ok"}, status=status.HTTP_200_OK)
        return Response(data={"message": "Something went wrong"}, status=status.HTTP_400_BAD_REQUEST)
