from django.views.generic import TemplateView


class TestTemplateView(TemplateView):
    template_name = "base.html"
