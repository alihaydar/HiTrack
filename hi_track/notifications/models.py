from django.db import models


class SmsLog(models.Model):
    phone_number = models.CharField(max_length=125)
    task_id = models.CharField(max_length=125, blank=True, null=True)
    req_text = models.TextField(blank=True, null=True)
