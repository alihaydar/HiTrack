from django.db.models.signals import post_save
from django.dispatch import receiver

from core.models import TimeLine, RepairJob
from .tools import handle_notifications
from .tasks import SendMail, SendSMS

import logging

logger = logging.getLogger(__name__)


@receiver(post_save, sender=TimeLine)
def repair_status_updated(sender, instance, created, raw, using, **kwargs):
    """
    Sends updates about repair status
    """
    logger.info('Timeline {} {}'.format(created, instance.id))
    if created:
        text = '{title}\n{description}\nYour ticker id is: {ticket_id}'.format(
            title=instance.repair_status.name,
            description=instance.repair_status.description,
            ticket_id=instance.repair_job.repairer_job_id
        )
        user = instance.repair_job.owner
        logger.info('Timeline number {}'.format(user.phone_number))
        mail = None
        if user.email:
            mail = user.email
        handle_notifications(text, phone=user.phone_number, mail=mail)


@receiver(post_save, sender=RepairJob)
def phone_attached_to_service(sender, instance, created, raw, using, **kwargs):
    """
    Sends notification to repairman
    """
    old_state = instance.old_object_state
    old_repair_service = old_state.get('repair_service')
    repair_service = instance.repair_service
    if repair_service:
        logging.info('repair_service.id {}'.format(repair_service.id))
        logging.info('old_repair_service {}'.format(old_repair_service))
        if repair_service.id != old_repair_service:

            device = instance.device
            repair_service = instance.repair_service
            service_phone = repair_service.phone_number
            service_mail = None
            customer_mail = None
            owner = instance.owner

            text_for_service = """
            We have a new device to repair. Problem {problem}.\n
            Platform: {platform}. Device type: {type}.\n
            Device: {device}
            """.format(
                problem=instance.problem_title,
                platform=device.os,
                type=device.device_type,
                device=device.name
            )

            text_for_customer = """
            A request for your device sent to service center at adress {address}.\n
            Service Center's name is {name}.\n
            Contact Mail: {mail}.\n
            Contact Phone: {phone} 
            """.format(
                address=repair_service.address,
                name=repair_service.name,
                phone=repair_service.phone_number,
                mail=repair_service.email
            )
            if repair_service.email:
                service_mail = repair_service.email
            if owner.email:
                customer_mail = owner.email
            handle_notifications(text_for_service, phone=service_phone, mail=service_mail)
            handle_notifications(text_for_customer, phone=owner.phone_number, mail=customer_mail)

    if instance.finish_date and old_state.get('finish_date') != instance.finish_date:
        owner = instance.owner
        customer_mail = None
        if owner.email:
            customer_mail = owner.email
        text_for_customer = "Your device may be ready this date {}. Ticket ID: ".format(instance.finish_date,
                                                                                       instance.repairer_job_id)
        handle_notifications(text_for_customer, phone=owner.phone_number, mail=customer_mail)
    if instance.cost and old_state.get('cost') != instance.cost:
        owner = instance.owner
        customer_mail = None
        if owner.email:
            customer_mail = owner.email
        text_for_customer = "Your service fee is {} azn. Ticket ID: ".format(instance.cost, instance.repairer_job_id)
        handle_notifications(text_for_customer, phone=owner.phone_number, mail=customer_mail)
