from django.conf import settings
from django.template import loader
from bs4 import BeautifulSoup
import uuid
import requests
from .models import SmsLog

from celery.task import Task


class SendSMS(Task):
    def run(self, msisdn, message):
        template_name = "sms.xml"

        context = {
            "login": settings.SMS_API_SETTINGS.get("login"),
            "password": settings.SMS_API_SETTINGS.get("password"),
            "title": settings.SMS_API_SETTINGS.get("title"),
            "controlid": str(uuid.uuid1()),
            "msisdn": msisdn,
            "message": message,
        }

        sms_xml = loader.get_template(template_name).render(context)
        req = requests.post(
            'https://sms.atatexnologiya.az/bulksms/api',
            data=sms_xml.encode('utf-8'), headers={'Content-Type': 'application/xml'})

        soup = BeautifulSoup(req.text, 'html.parser')
        if req.status_code == 200:
            sms_log = SmsLog.objects.create(phone_number=msisdn, req_text=req.text)
            if soup.find('responsecode').text == '000':
                sms_log.task_id = soup.find('taskid').text
                sms_log.save()
                return True

        return False


class SendMail(Task):
    def run(self, data):
        r = requests.post(
            "https://api.mailgun.net/v3/mail-server.safaroff.com/messages",
            auth=("api", "key-b6f424497aeaae7bc768fc07243b2f4b"),
            data=data)
        return r.status_code == 200
