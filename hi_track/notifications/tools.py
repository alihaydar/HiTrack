import logging

from .tasks import SendMail, SendSMS

logger = logging.getLogger(__name__)


def handle_notifications(text, mail=None, phone=None):
    logging.info('phone {}. mail {}'.format(phone, mail))
    if phone:
        SendSMS.delay(phone, text)
    # if user has email then also send email notification
    if mail:
        mail_context = {
            'from': 'infor@hiTrack.az',
            'to': mail,
            'subject': 'Hackathon/SafUsaglar',
            'text': text
        }
        SendMail.delay(mail_context)
