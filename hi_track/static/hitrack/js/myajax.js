$(function () {

    var $data_url = $('.get-data').attr('data-value');
//    rate ajax
    $('.rate-stars .fa-star').on('click', function () {
        var $data = $(this).attr('data-rate');

        var data_ob = {
            'rate': $data
        }
        $.ajax({
            url: $data_url,
            method: 'PUT',
            data: JSON.stringify(data_ob),
            success: function (data) {
                for (var i = 0; i <= $data; i++) {
                    $('span[data-rate=' + i + ']').addClass('checked');
                }
            }
        });
    });

    $('.send-comment').on('click', function (e) {
        e.preventDefault();
        $text = $('#comment').val();
        var my_text = {
            'comment': $text
        }

        $.ajax({
            url: $data_url,
            method: 'PUT',
            data: JSON.stringify(my_text),
            success: function (data) {
                $('#comment').val('')
                $('.user-comment').text(my_text.comment)
            }
        });
    });


    $('.agreed').on('click', function (e) {
        e.preventDefault();
        var $agreed = {
            'confirmed_by_owner': true
        }
        $.ajax({
            url: $data_url,
            method: 'PUT',
            data: JSON.stringify($agreed),
            success: function (data) {
                $('.agreed').css('display', 'none');
                $('.agreed-box').text('You agreed');
            }
        });
    });
});
